package com.example.catsapiretrofit_reinacarrascosacarlos

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter


class LoginActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Login()
        }
    }
}
@Composable
fun Login() {
    val context = LocalContext.current
    var user by remember { mutableStateOf("") }
    var passw by remember { mutableStateOf("") }
    var isValid by remember { mutableStateOf(false) }

    Column(
        modifier = Modifier.fillMaxSize().padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,

        ) {

        Image(
            painter = rememberAsyncImagePainter(model = "https://cdn2.thecatapi.com/images/a77.jpg"),
            contentDescription = "Imagen pantalla login",
            modifier = Modifier.size(250.dp).padding(10.dp).clip(RoundedCornerShape(20.dp)),
            contentScale = ContentScale.Crop
        )

        Text(
            text = "Cats API - Retrofit",
            fontSize = 32.sp)

        Spacer(modifier = Modifier.height(130.dp))


        OutlinedTextField(
            value = user,
            onValueChange =  {
                user = it
                isValid = user.isNotEmpty() && passw.isNotEmpty()
            },
            label = { Text("Username...") },
            modifier = Modifier.fillMaxWidth()
        )

        Spacer(modifier = Modifier.height(10.dp))


        OutlinedTextField(
            value = passw,
            onValueChange = {
                passw = it
                isValid = user.isNotEmpty() && passw.isNotEmpty()
            },
            label = { Text(text = "Password...") },
            visualTransformation = PasswordVisualTransformation(),
            modifier = Modifier.fillMaxWidth()
        )

        Spacer(modifier = Modifier.height(30.dp))

        Button(
            onClick = {
                val intent = Intent(context, MainActivity::class.java)
                context.startActivity(intent)

            },
            colors = ButtonDefaults.buttonColors(backgroundColor = Color.Magenta),
            modifier = Modifier.fillMaxWidth(),
            enabled = isValid
        ) {
            Text("Log in", color = Color.White)
        }
    }
}