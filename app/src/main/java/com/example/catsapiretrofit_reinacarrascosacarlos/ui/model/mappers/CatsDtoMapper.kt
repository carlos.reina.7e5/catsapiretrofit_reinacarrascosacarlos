package com.example.catsapiretrofit_reinacarrascosacarlos.ui.model.mappers

import com.example.catsapiretrofit_reinacarrascosacarlos.apiservice.model.BreedDto
import com.example.catsapiretrofit_reinacarrascosacarlos.apiservice.model.ImageDTO
import com.example.catsapiretrofit_reinacarrascosacarlos.ui.model.UIModel

@Suppress("UNREACHABLE_CODE", "UNUSED_PARAMETER")
class CatsDtoMapper {
    fun map(listBreeds : List<BreedDto>, listImages : List<ImageDTO>): List<UIModel>{
        return listBreeds.mapIndexed { _, breed ->
            UIModel(
                url = "",
                name = breed.name,
                desc = breed.description,
                countrycod = breed.countryCode,
                temperament = breed.temperament,
                urlwiki = breed.wikipediaUrl
            )
        }
        return mutableListOf<UIModel>().apply {
            listBreeds.forEachIndexed{
                    index, breed ->
                add(
                    UIModel(
                    url = listImages[index].url,
                    name = breed.name,
                    desc = breed.description,
                    countrycod = breed.countryCode,
                    temperament = breed.temperament,
                        urlwiki = breed.wikipediaUrl
                )
                )
            }
        }
    }

}
