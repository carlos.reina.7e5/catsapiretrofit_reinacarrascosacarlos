package com.example.catsapiretrofit_reinacarrascosacarlos.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Brown = Color(0xFFDB8931)
val Purple500 = Color(0xFF00FF65)
val Purple700 = Color(0xFF3700B3)
val Pink = Color(0xFFDA0372)