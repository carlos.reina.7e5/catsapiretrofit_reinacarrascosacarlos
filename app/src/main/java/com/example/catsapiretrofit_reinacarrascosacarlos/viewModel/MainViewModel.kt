package com.example.catsapiretrofit_reinacarrascosacarlos.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.catsapiretrofit_reinacarrascosacarlos.apiservice.CatsApi
import com.example.catsapiretrofit_reinacarrascosacarlos.ui.model.UIModel
import com.example.catsapiretrofit_reinacarrascosacarlos.ui.model.mappers.CatsDtoMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {
    private val _catsUIState = MutableStateFlow<List<UIModel>?>(null)
    val catsUIState: StateFlow<List<UIModel>?> = _catsUIState.asStateFlow()

    private var mapper = CatsDtoMapper()


    init{
        getCats()
    }

    private fun getCats() {
        viewModelScope.launch {
            val breedList = CatsApi.retrofitService.getBreed()
            _catsUIState.value = mapper.map(breedList, emptyList())
        }
    }
}