package com.example.catsapiretrofit_reinacarrascosacarlos.apiservice.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Suppress("PLUGIN_IS_NOT_ENABLED")
@Serializable
data class WeightDTO(
    @SerialName("imperial") var imperial: String,
    @SerialName("metric") var metric: String
)