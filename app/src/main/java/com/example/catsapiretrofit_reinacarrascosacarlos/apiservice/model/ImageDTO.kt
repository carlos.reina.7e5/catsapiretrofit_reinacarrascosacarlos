package com.example.catsapiretrofit_reinacarrascosacarlos.apiservice.model
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Suppress("PLUGIN_IS_NOT_ENABLED")
@Serializable
data class ImageDTO (
        @SerialName("id") var id: String,
        @SerialName("url") var url: String ? = "",
        @SerialName("width") var width: Int,
        @SerialName("height") var height: Int
        )

