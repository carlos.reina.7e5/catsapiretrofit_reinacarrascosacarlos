package com.example.catsapiretrofit_reinacarrascosacarlos.apiservice

import com.example.catsapiretrofit_reinacarrascosacarlos.apiservice.model.BreedDto

import com.example.catsapiretrofit_reinacarrascosacarlos.apiservice.model.ImageDTO
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private const val BASE_URL = "https://api.thecatapi.com/"
private val retrofit = Retrofit.Builder()
    .addConverterFactory(GsonConverterFactory.create())
    .baseUrl(BASE_URL)
    .build()

interface MarsApiService {
    @GET("./v1/breeds")
    suspend fun getBreed() : List<BreedDto>
    @GET("./v1/images")
    suspend fun getPhotos(@Query("breed_id")id:String) : List<ImageDTO>
}

object CatsApi {
    val retrofitService : MarsApiService by lazy {
        retrofit.create(MarsApiService::class.java)
    }
}