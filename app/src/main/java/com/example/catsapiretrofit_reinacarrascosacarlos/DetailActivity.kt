package com.example.catsapiretrofit_reinacarrascosacarlos

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.example.catsapiretrofit_reinacarrascosacarlos.ui.model.UIModel


@Suppress("DEPRECATION")
class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val info = intent.getParcelableExtra<UIModel>("cat")

        setContent {
            if (info != null) {
                AffirmationAppDetail(info)
            }
        }

    }
}

@Composable
fun AffirmationAppDetail(cat: UIModel) { AffirmationDetail(cat) }

@SuppressLint("SuspiciousIndentation")
@Composable
fun AffirmationDetail(cat: UIModel) {
        LocalContext.current
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxWidth()
        ) {
            Image(
                painter = rememberAsyncImagePainter(model = "https://cdn2.thecatapi.com/images/chk.jpg"),
                contentDescription = "Imgen del gato",
                modifier = Modifier.padding(13.dp).size(200.dp).clip(RoundedCornerShape(50)),
                contentScale = ContentScale.Crop,
                )

            Text(
                text = cat.name,
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.h3,
                modifier = Modifier.padding(10.dp)
            )

            Text(
                text = cat.desc,fontSize = 18.sp,
                style = MaterialTheme.typography.body1,
                modifier = Modifier.padding(15.dp)
            )

            if( cat.countrycod != null){
                Text(
                    text = cat.countrycod,
                    style = MaterialTheme.typography.h6,
                    modifier = Modifier.padding(13.dp),
                    fontWeight = FontWeight.Bold
                )
            }

            Text(
                text = cat.temperament,
                style = MaterialTheme.typography.h6,
                modifier = Modifier.padding(13.dp),
                fontStyle = FontStyle.Italic,
                fontSize = 17.sp
            )
        }
    }
