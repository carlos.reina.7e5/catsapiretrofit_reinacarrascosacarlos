@file:Suppress("NAME_SHADOWING")

package com.example.catsapiretrofit_reinacarrascosacarlos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.catsapiretrofit_reinacarrascosacarlos.viewModel.MainViewModel
import com.example.catsapiretrofit_reinacarrascosacarlos.ui.model.UIModel

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (supportActionBar != null) {
            supportActionBar!!.hide()
        }
        setContent{
            App()
        }
    }
}

@Composable
fun App(catViewModel: MainViewModel = viewModel()) {
    val uiState by catViewModel.catsUIState.collectAsState()
    uiState?.let {
        List(catsList = it)
    }
}


@Composable
fun List(catsList: List<UIModel>, modifier: Modifier = Modifier) {
    LazyColumn {
        this.items(items = catsList, itemContent = { item ->
            Card(cat = item)
        })
    }
}


@Composable
fun Card(cat: UIModel, modifier: Modifier = Modifier) {
    val expanded = remember { mutableStateOf(false) }
    var backgroundColor = if (expanded.value) Color.Gray else Color.White
    androidx.compose.material.Card(
        modifier = Modifier.fillMaxSize().padding(vertical = 5.dp).background(color = backgroundColor)
            .clickable {
                expanded.value = !expanded.value
                backgroundColor = if (expanded.value) Color.Gray else Color.White
            },
    ) {
        Column(
            modifier = Modifier.animateContentSize(
                animationSpec = spring(
                    dampingRatio = Spring.DampingRatioMediumBouncy,
                    stiffness = Spring.StiffnessLow
                )
            )
        ) {
            Row {
                Image(
                    painter = rememberAsyncImagePainter(model = "https://cdn2.thecatapi.com/images/chk.jpg"),
                    contentDescription = "Imagen del gato",
                    modifier = Modifier.padding(10.dp).size(70.dp).clip(RoundedCornerShape(20)),
                    contentScale = ContentScale.Crop
                )
                Text(
                    text = cat.name,
                    textAlign = TextAlign.Center,
                    fontSize = 25.sp,
                    modifier = Modifier.padding(top = 13.dp, end = 2.dp).weight(1f)
                )
                ExpandButton(
                    expanded = expanded.value,
                    onClick = {
                        expanded.value = !expanded.value
                    })

            }
            if (expanded.value) {
                Description(cat)
            }
        }
    }
}

@Composable
fun ExpandButton(
    expanded: Boolean, onClick: () -> Unit, modifier: Modifier = Modifier
) {
    IconButton(onClick = onClick) {
        Icon(
            imageVector =
            if (expanded)
                Icons.Filled.Close
            else
                Icons.Filled.Add,
            tint = MaterialTheme.colors.secondaryVariant,
            contentDescription = "Boton para ver mas informacion",
            modifier = Modifier.size(60.dp)
        )
    }

}

@Composable
fun Description(cat: UIModel, modifier: Modifier = Modifier) {
    LocalContext.current
    Column(
        modifier = modifier.padding(
            start = 18.dp, top = 8.dp, bottom = 15.dp, end = 16.dp
        )
    ) {

        Text(
            overflow = TextOverflow.Ellipsis,
            maxLines = 2,
            text = cat.desc,
            style = MaterialTheme.typography.body1,
        )
        val context = LocalContext.current
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.Magenta)
                .clickable(onClick = {
                    val intent = Intent(context, DetailActivity::class.java)
                    intent.putExtra("cat", cat)
                    context.startActivity(intent)
                }),
            contentAlignment = Alignment.Center
        ) {
            Text("See more", color = Color.White)
        }
    }
}